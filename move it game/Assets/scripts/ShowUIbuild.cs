﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowUIbuild : MonoBehaviour {
    
    public GameObject[] uiObjects;
    public AccelerometerInput script;
    
    void Start()
    {
        script = GameObject.Find("Player").GetComponent<AccelerometerInput>();
        script.coinAmount = 0;
    }
    // Update is called once per frame
    void Update()
    {
        if (script.coinAmount == 100) 
        {
            uiObjects[0].SetActive(true);
            Destroy(uiObjects[0],3);
          
        }
        
        else if (script.coinAmount == 500) 
        {
            uiObjects[1].SetActive(true);
            Destroy(uiObjects[1],3);
        }
        
        else if (script.coinAmount == 1000)
        {
            uiObjects[2].SetActive(true);
            Destroy(uiObjects[2],3);
        }
        
        else if (script.coinAmount == 5000) 
        {
            uiObjects[3].SetActive(true);
            Destroy(uiObjects[3],10);
            
        }
    }
}