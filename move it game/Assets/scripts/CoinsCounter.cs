﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsCounter : MonoBehaviour
{
    private int coinAmount;
    
    [SerializeField] 
    private Text coinCounter;
    
    // Start is called before the first frame update
    void Start()
    {
        coinAmount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        coinCounter.text = "Coins: " + coinAmount.ToString();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.GetComponent<Coin>())
        {
            coinAmount += 1;
            Destroy(collision.gameObject);
        }
        else if (collision.GetComponent<Diamond>())
        {
            coinAmount += 100;
            Destroy(collision.gameObject);
        }
    }

  
}
