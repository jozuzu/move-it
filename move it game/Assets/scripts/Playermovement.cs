﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour
{

    public CharacterController controllerPlayer;
    public float speed = 5;

    void Start()
    {
        controllerPlayer = GetComponent<CharacterController>();
    }
    
    void Update()
    {
        Vector3 mov = new Vector3(SimpleInput.GetAxis("Horizontal") * speed, 0, SimpleInput.GetAxis("Vertical") * speed);
        controllerPlayer.Move(mov * Time.deltaTime);
    }
}