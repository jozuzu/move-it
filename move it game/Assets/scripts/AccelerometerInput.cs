﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AccelerometerInput : MonoBehaviour
{
    
    // Move object using accelerometer
    float accelerometerUpdateinterval = 1.0f / 60.0f;

    float lowPassKernelWidthInSeconds = 1.0f;
    float shakeDetectionThreshold = 2.0f;

    float lowPassFilterFactor;
    public AccelerometerInput script;
    Vector3 lowPassValue;
    public int coinAmount;

    [SerializeField] 
    public Text coinCounter;
    
    // Start is called before the first frame update
    void Start()
    {
        coinAmount = 0;
        lowPassFilterFactor = accelerometerUpdateinterval / lowPassKernelWidthInSeconds;
        shakeDetectionThreshold *= shakeDetectionThreshold;
        lowPassValue = Input.acceleration;
    }

    // Update is called once per frame
    void Update()
    {
        coinCounter.text = "Coins: " + coinAmount.ToString();
        Vector3 acceleration = Input.acceleration;
        lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
        Vector3 deltaAcceleration = acceleration - lowPassValue;

        if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold)
        {
            coinAmount += 1;
        }
        
    }
  

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.GetComponent<Coin>())
        {
            coinAmount += 1;
            Destroy(collision.gameObject);
        }
        else if (collision.GetComponent<Diamond>())
        {
            coinAmount += 50;
            Destroy(collision.gameObject);
        }
    }
}
