﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBuilding : MonoBehaviour
{
    
    public AccelerometerInput script;
    public GameObject[] prefabs;

    void Start()
    {
        script = GameObject.Find("Player").GetComponent<AccelerometerInput>();
        script.coinAmount = 0;
    }

    void Update()
         {
             if (script.coinAmount == 100) 
             {
                 Instantiate(prefabs[0], new Vector3(9, 0, 15), Quaternion.identity);

             }
             else if (script.coinAmount == 500) 
             {
                 Instantiate(prefabs[1], new Vector3(-7, 0, 8), Quaternion.identity);
             }
             else if (script.coinAmount == 1000)
             {
                 Instantiate(prefabs[2], new Vector3(0, 0, 30), Quaternion.identity);
             }
             
         }
}