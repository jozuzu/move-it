﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class UpdateAccelerometer : MonoBehaviour
{

     float accelerometerUpdateinterval = 1.0f / 60.0f;

     float lowPassKernelWidthInSeconds = 1.0f;
     float shakeDetectionThreshold = 2.0f;

     float lowPassFilterFactor;
     public AccelerometerInput script;
     Vector3 lowPassValue;
    // Start is called before the first frame update
    void Start()
    {
    
        lowPassFilterFactor = accelerometerUpdateinterval / lowPassKernelWidthInSeconds;
        shakeDetectionThreshold *= shakeDetectionThreshold;
        lowPassValue = Input.acceleration;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 acceleration = Input.acceleration;
        lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
        Vector3 deltaAcceleration = acceleration - lowPassValue;

        if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold)
        {
            script.coinAmount += 1;
        }
    }
}
